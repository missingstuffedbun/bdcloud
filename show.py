from __future__ import print_function

import sys

#不用import有的没的，直接用装好的dict
from bddown_core import Pan
from util import bd_help

#其实直接show(dict)会比较爽，这部分跟util那边连起来
def show(links):
    if not len(links):
        bd_help('show')
    else:
        for url in links:
            pan = Pan()
            info = pan.get_dlink(url)
            print(u"{0}\n{1}\n\n".format(info.filename, info.dlink).encode('utf-8'))
    sys.exit(0)
